<?php

// Set the namespace for this file
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class DefaultModel extends AbstractModel{
    protected static $table = 'contacts';
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    protected $sujet;

    /**
     * @return mixed
     */
    public function getSujet()
    {
        return $this->sujet;
    }
    protected $email;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }
    protected $message;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
    protected $created_at;

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (sujet, email, message) VALUES (?,?,?)",
            array($post['sujet'], $post['email'], $post['message'])
        );
    }
}