<?php

namespace App\Controller;

use App\Model\DefaultModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';
        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'message' => $message,
        ));
    }

    public function listing(){
        $message = 'list des contacts';

        $this->render('app.default.listing', array(
            'contacts' => DefaultModel::all(),
            'message' => $message,
        ));
    }

    public function add(){
        $errors = array();
        if (!empty($_POST['submitted'])){
            //faille xss
            $post = $this->cleanXss($_POST);
            //validation
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if ($v->IsValid($errors)){
                DefaultModel::insert($post);
                //flash mssg
                $this->addFlash('success','nouvelle contact ajouté');
                //redirection
                $this->redirect('contact_list');
            }
        }
        $form = new Form($errors);
        $this->render('app.default.add', array(
            'form' => $form,
            'contacts' => DefaultModel::all()
        ));

    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
    private function validate($v,$post)
    {
        $errors = [];
        $errors['sujet'] = $v->textValid($post['sujet'], 'sujet',3, 150);
        $errors['email'] = $v->textValid($post['email'], 'email',3, 150);
        $errors['message'] = $v->textValid($post['message'], 'message',3, 500);
        return $errors;
    }


}
